import { POINT_LIST, QUESTION_LVL_LIST } from "@/const";

export const getQuestionLvl = (lvl: number) => {
  return QUESTION_LVL_LIST.find((i) => i.value === lvl);
};

export const getPoint = (point: number) => {
  return POINT_LIST.find((i) => i.value === point);
};
