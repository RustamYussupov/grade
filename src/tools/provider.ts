import axios from "axios";

axios.defaults.baseURL = process.env.VUE_APP_API;

axios.interceptors.request.use(
  function (config) {
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
    };
    const token = localStorage.getItem("access_token");

    if (token) {
      config.headers = {
        Authorization: `Bearer ${token}`,
        ...headers,
      };
    } else {
      config.headers = {
        ...headers,
      };
    }

    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default axios;
