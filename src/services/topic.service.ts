import axios from "../tools/provider";

const ENDPOINT = "/topics/";

const getTopicList = (params: any = {}) => {
  return axios
    .get(ENDPOINT, {
      params,
    })
    .then((res) => res.data);
};

const createTopic = (body: any) => {
  return axios.post(ENDPOINT, body).then((res) => res.data);
};

const updateTopic = (id: number, body: any) => {
  return axios.patch(ENDPOINT + id, body).then((res) => res.data);
};

export default {
  getTopicList,
  createTopic,
  updateTopic,
};
