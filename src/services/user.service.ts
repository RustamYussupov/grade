import axios from "../tools/provider";

const ENDPOINT = "/users/";

const getUserList = (params: any = {}) => {
  return axios
    .get(ENDPOINT, {
      params,
    })
    .then((res) => res.data);
};

const createUser = (body: any) => {
  return axios.post(ENDPOINT, body).then((res) => res.data);
};

const updateUser = (id: number, body: any) => {
  return axios.patch(ENDPOINT + id, body).then((res) => res.data);
};

export default {
  getUserList,
  createUser,
  updateUser,
};
