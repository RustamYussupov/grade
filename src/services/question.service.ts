import axios from "../tools/provider";

const ENDPOINT = "/questions/";

const getQuestionList = (params: any = {}) => {
  return axios
    .get(ENDPOINT, {
      params,
    })
    .then((res) => res.data);
};

const createQuestion = (body: any) => {
  return axios.post(ENDPOINT, body).then((res) => res.data);
};

const updateQuestion = (id: number, body: any) => {
  return axios.patch(ENDPOINT + id, body).then((res) => res.data);
};

export default {
  getQuestionList,
  createQuestion,
  updateQuestion,
};
