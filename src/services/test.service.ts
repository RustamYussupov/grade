import axios from "../tools/provider";

const ENDPOINT = "/tests/";

const getTestList = (params: any = {}) => {
  return axios
    .get(ENDPOINT, {
      params,
    })
    .then((res) => res.data);
};

const getTest = (id: number) => {
  return axios.get(`${ENDPOINT}${id}/`).then((res) => res.data);
};

const createTest = (body: any) => {
  return axios.post(ENDPOINT, body).then((res) => res.data);
};

const updateTest = (id: number, body: any) => {
  return axios.patch(ENDPOINT + id, body).then((res) => res.data);
};

export default {
  getTestList,
  getTest,
  createTest,
  updateTest,
};
