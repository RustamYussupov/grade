import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import HomeView from "../views/HomeView.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/topics",
    name: "topics",
    component: () =>
      import(/* webpackChunkName: "topics-view" */ "../views/TopicsView.vue"),
  },
  {
    path: "/questions/:topicId",
    name: "questions",
    component: () =>
      import(
        /* webpackChunkName: "questions-view" */ "../views/QuestionsView.vue"
      ),
  },
  {
    path: "/user/:userId/test",
    name: "test",
    component: () =>
      import(/* webpackChunkName: "test-view" */ "../views/TestView.vue"),
  },
  {
    path: "/user/:userId/test/:testId",
    name: "test-update",
    component: () =>
      import(
        /* webpackChunkName: "test-update-view" */ "../views/TestView.vue"
      ),
  },
  {
    path: "/user/tests/:userId",
    name: "user-tests",
    component: () =>
      import(
        /* webpackChunkName: "user-tests-view" */ "../views/UserTestsView.vue"
      ),
  },
  {
    path: "/user/test-result/:testId",
    name: "user-test-result",
    component: () =>
      import(
        /* webpackChunkName: "user-test-result-view" */ "../views/UserTestView.vue"
      ),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
